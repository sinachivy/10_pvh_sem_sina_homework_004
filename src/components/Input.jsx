import React, { Component } from "react";
import "flowbite";
import TableData from "./TableData";
export class Input extends Component {
  constructor() {
    super();
    this.state = {
      iu_Email: "null",
      in_Username: "null",
      in_Age: "null",
      in_Status: "Pending",
      myInfo: [
        {
          id: 1,
          email: "sinaChivy",
          userName: "sina",
          age: 21,
          status: "Pending",
        },
        {
          id: 2,
          email: "sinasani",
          userName: "sina",
          age: 21,
          status: "Pending",
        },
        {
          id: 3,
          email: "sinacoca",
          userName: "sina",
          age: 21,
          status: "Pending",
        },
      ],
    };
  }

  acceptData = (event) => {
    this.setState({
      // newInfo: event.target.value,
      [event.target.name]: event.target.value,
      //catch user input
    });
  };
  onSubmit = () => {
    const newObj = {
      id: this.state.myInfo.length + 1,
      email: this.state.iu_Email,
      userName: this.state.in_Username,
      age: this.state.in_Age,
      status: this.state.in_Status,
    };

    this.setState({
      myInfo: [...this.state.myInfo, newObj],
      newInfo: "",
    });
  };

  showAction = (e) => {
    var data =this.state.myInfo;
    
    data.map((i)=>{
      if(i.id==e){
        i.status=i.status ==="Pending" ?"Done":"Pending"
      }
      
    }
    )
    this.setState(
      {
        data
      }
    )
  }
  
  
  render() {
    return (
      <div class="bg-[#E7E6E1]">
        <h1
          class="font-extrabold text-transparent text-6xl text-align-center bg-clip-text bg-gradient-to-r from-purple-400 to-pink-600  "
          id="head"
        >
          Please Fill Your Information
        </h1>
        <div class="form ">
          <div class="mb-6 w-1/2 ">
            <label
              for="email"
              class="block mb-2 text-sm font-medium text-black-900 dark:text-white mt-20"
            >
              <i class="fa-brands fa-google"> Your email</i>
            </label>
            <input
              onChange={this.acceptData}
              type="email"
              id="email"
              class="bg-gray-50 border border-[#F2A154] text-grey-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5  dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
              placeholder="email"
              name="iu_Email"
            />
          </div>
          <div class="mb-6 w-1/2">
            <label
              for="username"
              class=" block mb-2 text-sm font-medium text-black-900 dark:text-white mt-2"
            >
              Username
            </label>
            <input
              type="username"
              id="username"
              class=" border border-[#F2A154] text-grey-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
              placeholder="username"
              required
              name="in_Username"
              onChange={this.acceptData}
            />
          </div>
          <div class="mb-6 w-1/2">
            <label
              for="Age"
              class="block mb-2 text-sm font-medium text-black-900 dark:text-white mt-2"
            >
              Age
            </label>
            <input
              type="Age"
              id="Age"
              class="bg-gray-50 border border-[#F2A154] text-grey-900 text-sm rounded-lg focus:ring-blue-500 focus:border-[#00B7A8] block w-full p-2.5     "
              placeholder="Age"
              required
              name="in_Age"
              onChange={this.acceptData}
            />
          </div>
          <button
            // type="button"
            onClick={this.onSubmit}
            class="text-white bg-gradient-to-r from-purple-500 to-pink-500 hover:bg-gradient-to-l focus:ring-4 focus:outline-none focus:ring-purple-200 dark:focus:ring-purple-800 font-medium rounded-lg text-sm px-5  py-4 px-8
              mr-2 mb-2 mt-5  "
          >
            Register
          </button>
        </div>
        <TableData
          data={this.state.myInfo}
          changeStatus={this.showAction}
        />
      </div>
    );
  }
}

export default Input;
