import React, { Component } from 'react'
import 'animate.css'
import Swal from 'sweetalert2'

export class TableData extends Component {
    constructor(props) {
      super(props)
    
      console.log(props);
    }
 
     
    sweetAlert = (data) =>{
        console.log(data);
        Swal.fire({
            title: "\nId : " + data.id + "\nEmail: "  + data.email + "\nUserName : " + data.userName + "\nAge : " + data.age ,
            showClass: {
              popup: 'animate__animated animate__fadeInDown'
            },
            hideClass: {
              popup: 'animate__animated animate__fadeOutUp'
            }
          })
    }
  render() {
    return (
      <div>

<div class=" relative overflow-x-auto shadow-md sm:rounded-lg bg-white-900 mt-10   "id="table">
    <table class="w-full  text-left text-gray-900 text-black  mt-10 ">
        <thead class="text-xs text-gray-700 uppercase bg-[#EABF9F]  ">
            <tr class="">
                <th scope="col" class="px-6 py-5">
                   ID
                </th>
                <th scope="col" class="px-6 py-5">
                    Email
                </th>
                <th scope="col" class="px-6 py-5">
                    username
                </th>
                <th scope="col" class="px-6 py-5">
                    age
                </th>
                <th scope="col" class="px-6 py-5 text-center">
                    status
                </th>
            </tr>
        </thead>
      <tbody class="">
        
       {
        this.props.data.map((i)=>(
            <tr class="even:bg-[#BFACE2] odd:bg-[#E3DFFD]">
                <td >{i.id}</td>
                <td>{i.email}</td>
                <td>{i.userName}</td>
                <td>{i.age}</td>
                <td>
                <button  
                onClick={()=>this.props.changeStatus(i.id) }
                type="button" class="text-white bg-gradient-to-br from-purple-500 to-blue-400 hover:bg-gradient-to-bl focus:ring-4
                 focus:outline-none focus:ring-blue-300 dark:focus:ring-blue-800 font-medium rounded-lg text-sm px-7 py-4 text-center mr-2 mb-2 ml-20 mt-1"> {i.status}
                 </button>
                <button onClick={()=>this.sweetAlert(i)} type="button" class="text-grey-900 bg-gradient-to-r from-purple-400 to-pink-400 hover:bg-gradient-to-l
                 focus:ring-4 focus:outline-none focus:ring-purple-200 dark:focus:ring-purple-800 font-medium rounded-lg text-sm px-7 py-4 text-center mr-2 mb-2  " >
                  Show me</button>
                </td>
                
            </tr>
        )
            
        )
       }
      </tbody>
    </table>
</div>

      </div>
    )
  }
}

export default TableData